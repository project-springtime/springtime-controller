/**
 * Concept of a State Machine and all constituent parts.
 */

#include "StateMachine.hh"
#include <stdio.h>

// State

State::State(
        const char * name )
    :
        theName( name )
{}

void State::enter(
        const ExecutionContext * contextP )
{
    printf( "State: entering %s (%p)\n", theName, this );
}

void State::execute(
        const ExecutionContext * contextP )
{
    // printf( "State: executing %s (%p)\n", theName, this );
}

void State::leave(
        const ExecutionContext * contextP )
{
    printf( "State: leaving %s (%p)\n", theName, this );
}

State::~State()
{}

// HierarchicalState

HierarchicalState::HierarchicalState(
        const char * name,
        State      * childStateP )
    :
        State( name ),
        theChildStateP( childStateP )
{}

void HierarchicalState::transitionTo(
        State                  * newStateP,
        const ExecutionContext * contextP )
{
    printf( "HierarchicalState::transitionTo from %s (%p) -> %s (%p)\n", theChildStateP->getName(), theChildStateP, newStateP->getName(), newStateP );

    theChildStateP->leave( contextP );
    theChildStateP = newStateP;
    theChildStateP->enter( contextP );
}

void HierarchicalState::enter(
        const ExecutionContext * contextP )
{
    State::enter( contextP );
    theChildStateP->enter( contextP );
}

void HierarchicalState::execute(
        const ExecutionContext * contextP )
{
    State::execute( contextP );
    theChildStateP->execute( contextP );
}

void HierarchicalState::leave(
        const ExecutionContext * contextP )
{
    State::leave( contextP );
    theChildStateP->leave( contextP );
}
