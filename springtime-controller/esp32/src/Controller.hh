/**
 * The central controller of the behavior of the system
 */

#pragma once

#include "Parameters.hh"
#include "StateMachine.hh"
#include "Pump.hh"

class Controller
{
    StateMachine * theStateMachineP;
    Pump         * thePumpP;
    Parameters   * theParametersP;

public:
    Controller(
            StateMachine * stateMachineP,
            Pump         * pumpP,
            Parameters   * parsP )
        :
            theStateMachineP( stateMachineP ),
            thePumpP(         pumpP ),
            theParametersP(   parsP )
    {}

    Parameters * getParametersP()
    {
        return theParametersP;
    }

    void init(
            long currentTime )
    {
        ExecutionContext context( theParametersP, currentTime );

        thePumpP->init();
        theStateMachineP->init( &context );
    }

    void execute(
            long currentTime )
    {
        ExecutionContext context( theParametersP, currentTime );

        theStateMachineP->execute( &context );
    }

    static const char * const PAR_PUMP_DUTY;
    static const char * const PAR_PUMP_ON;
    static const char * const PAR_PUMP_PERIOD_DAY;
};


extern Controller CONTROLLER;
