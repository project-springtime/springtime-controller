
#pragma once

#include "Parameter.hh"
#include <cstring>
#include <stdio.h>

class Parameters {
private:
    const int         theNPars;
    Parameter * const thePars;

public:
    Parameters(
            const int         nPars,
            Parameter * const pars );

    int get(
            const char * name ) const;

    void set(
            const char * name,
            const int    newValue );

    void printList();

    void loadFromNvs();

    void saveToNvs();

    /* all parameters, not just the current ones */
    void deleteAllFromNvs();
};

