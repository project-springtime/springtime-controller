/**
 * The controller task, and everything run from there
 */

#include "Controller.hh"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

extern "C"
void controller_task()
{
    // for( int i=5 ; i>=0 ; --i ) {
    //     printf( "Delaying controller task %d...\n", i );
    //
    //     vTaskDelay( 1000 / portTICK_PERIOD_MS );
    // }

    const TickType_t xDelay        = 1000 / portTICK_PERIOD_MS; /* run once a second */
    TickType_t       xLastWakeTime = xTaskGetTickCount();

    CONTROLLER.init( xLastWakeTime * portTICK_PERIOD_MS );

    while( 1 ) {
        xLastWakeTime = xTaskGetTickCount();

        CONTROLLER.execute( xLastWakeTime * portTICK_PERIOD_MS );

        vTaskDelayUntil( &xLastWakeTime, xDelay );
    }
}

