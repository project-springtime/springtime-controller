#pragma once

class Parameter {
private:
    const char * theName;
    int          theValue;

public:
    Parameter(
            const char * name,
            int          value = -1 )
        :
            theName( name ),
            theValue( value )
    {}

    const char * getName() const
    {
        return theName;
    }

    int get() const
    {
        return theValue;
    }

    void set(
            const int newValue )
    {
        theValue = newValue;
    }
};

