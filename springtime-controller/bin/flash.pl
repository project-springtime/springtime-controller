#!/usr/bin/perl
#
# Flash the ESP32
#

use strict;

use UBOS::Logging;
use UBOS::Utils;

my $ret = 1;

if( 'deploy' eq $operation ) {
    my $dev = $config->getResolve( 'installable.customizationpoints.esp32tty.value' );

    info( 'Flashing ESP32 at', $dev );

    my $cmd = "/usr/bin/esptool.py";
    $cmd .= " --chip esp32";
    $cmd .= " --port $dev";
    $cmd .= " --baud 460800";
    $cmd .= " --before default_reset";
    $cmd .= " --after hard_reset";
    $cmd .= " write_flash";
    $cmd .= "    -z";
    $cmd .= "   --flash_mode dio";
    $cmd .= "   --flash_freq 40m";
    $cmd .= "   --flash_size detect";
    $cmd .= " 0x1000 /ubos/share/springtime-controller/esp32/bootloader.bin";
    $cmd .= " 0x8000 /ubos/share/springtime-controller/esp32/partitions.bin";
    $cmd .= " 0x10000 /ubos/share/springtime-controller/esp32/firmware.bin";

    my $out;
    if( UBOS::Utils::myexec( $cmd, undef, \$out, \$out )) {
        error( 'Failed to flash ESP32:', $out );
        $ret = 0;
    }

    info( 'Done flashing ESP32 at', $dev );
}

$ret;
